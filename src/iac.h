#pragma once
#include <stdio.h>
#include <string.h>

#include <string>
#include <map>
#include <deque>
#include <stack>

#include <iostream>
#include <functional>
#include <algorithm>

#include <fstream>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/deque.hpp>
#include <boost/serialization/string.hpp>

class CItem;

void AddItemToContainerIfRequired(CItem *item);