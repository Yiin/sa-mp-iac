#include "Container.h"
#include <sampgdk/sampgdk.h>

std::map<pair<int, int>, CContainer> gContainers;

int CContainer::getCount() const { 
	return items.size();
}

void RemoveItemFromContainer(CItem * item) {
	if (item->getContainer() != NULL) {
		gContainers[make_pair(item->getA(), item->getB())].Remove(item);
	}
}

bool CContainer::Add(CItem * item, int a, int b, bool already_exists) {
	if(item->getContainer() != NULL && already_exists == false) {
		gContainers[make_pair(item->getA(), item->getB())].Remove(item);
	}
	item->setContainer(a, b);
	items.push_front(item);

	return true;
}

void CContainer::Remove(CItem * item) { 
	deque<CItem *>::iterator result;

	result = std::find(items.begin(), items.end(), item); 

	if(result != items.end()) {
		items.erase(result);
	}
}

int CContainer::GetAt(int index) {
		if(index < getCount()) {
			return items[index]->getUniqueId();
		}
		return NULL;
	}

CItem * CContainer::Find(int idx) { 
	for(auto i = items.begin(); i != items.end(); ++i) {
		if((*i)->getUniqueId() == idx) {
			return *i;
		}
	}
	return NULL;
}


CItem * CContainer::Find(string item, int nth) { 
	int xth = 0;
	for(auto i = items.begin(); i != items.end(); ++i) {
		if((*i)->getId() == item && xth++ == nth) {
			return *i;
		}
	}
	return NULL;
}

void CContainer::Delete() {
	for(auto i = items.begin(); i != items.end(); ++i) {
		CItem::DeleteItem(*i);
	}
	items.clear();
}

CContainer::CContainer(void)
{	
}


CContainer::~CContainer(void)
{
	// Delete();
}
