#pragma once
#include "Item.h"

class CContainer
{
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, unsigned int version)
	{
		ar & items;
	}
	deque<CItem *> items;

public:
	int getCount() const;
	bool Add(CItem * item, int a, int b, bool already_exists = false);
	void Remove(CItem * item);
	int GetAt(int index);
	
	CItem * Find(int idx);
	CItem * Find(string item, int nth = 0);

	void Delete();

	CContainer(void);
	~CContainer(void);
};

extern std::map<pair<int, int>, CContainer> gContainers;

void RemoveItemFromContainer(CItem * item);