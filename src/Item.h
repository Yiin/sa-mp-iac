#pragma once
#include "iac.h"

using namespace std;

class CItem
{
public:
	static void DeleteItem(CItem* item);
	static CItem * GetItemByUID(int uid);
private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, unsigned int /* version */)
	{
		ar & itemType;
		ar & itemId;
		ar & itemLevel;
		ar & itemSize;
		ar & itemInfo;
		ar & itemEffects;
		ar & itemContainerA;
		ar & itemContainerB;
	}
	unsigned int uniqueId;
	
	string itemId;
	int itemType;
	map<string, int> itemInfo;
	map<int, int> itemEffects;
	short int itemLevel;
	int itemSize;
	int itemContainerA;
	int itemContainerB;
public:
	int						getUniqueId()					const { return uniqueId; }
	int						getType()						const { return itemType; }
	string					getId()							const { return itemId; }
	short int				getLevel()						const { return itemLevel; }
	int						getSize()						const { return itemSize; }
	map<int, int>	getEffects()					const { return itemEffects; }
	int						countEffects()					const { int count=0;for(auto i=itemEffects.begin();i!=itemEffects.end();++i)if(i->second!=0)++count;return count; }
	int						getEffect(int effect)			{ return itemEffects[effect]; }
	int						getInfo(string info)			{ return itemInfo[info]; }
	bool					getContainer()					const { return itemContainerA != NULL; }
	int						getA()							const { return itemContainerA; }
	int						getB()							const { return itemContainerB; }

	CItem * getPtr() { return this; }

	void setLevel(int level) { itemLevel = level; }
	void setType(int type) { itemType = type; }
	void setEffect(int effect, int value) { itemEffects[effect] = value; }
	void setInfo(string info, int value) { itemInfo[info] = value; }
	void setContainer(int a, int b) { itemContainerA = a; itemContainerB = b; }

	bool operator == (const CItem * r) {
		return r->getId() == getId() && r->getLevel() == getLevel() && r->getEffects() == getEffects();
	}

	void CreateItem();
	
	CItem();
	CItem(string id);
	CItem(string id, int count);
	CItem(string id, int count, int level);
	~CItem(void);
};

extern std::map<int, CItem *> gUidBasedList;
extern std::deque<CItem *> gItems;
extern std::stack<int> gUnusedUniqueIds;