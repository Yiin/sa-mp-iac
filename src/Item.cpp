#include "Item.h"
#include "Container.h"
#include <sampgdk/sampgdk.h>

std::deque<CItem *> gItems;
std::stack<int> gUnusedUniqueIds;
std::map<int, CItem *> gUidBasedList;

CItem * CItem::GetItemByUID(int uid)
{
	return gUidBasedList[uid];
}

void CItem::DeleteItem(CItem * item)
{
	deque<CItem *>::iterator result;

	result = std::find(gItems.begin(), gItems.end(), item); 

	if(result != gItems.end()) {
		gUnusedUniqueIds.push(item->getUniqueId());
		gUidBasedList.erase(item->getUniqueId());
		if (item->getContainer() != NULL) {
			RemoveItemFromContainer(item);
		}
		gItems.erase(result);
		delete item;
	}
}

void CItem::CreateItem()
{
	static unsigned int nextUniqueId = 1; 
	if(gUnusedUniqueIds.empty()) {
		uniqueId = nextUniqueId++;
	}
	else {
		uniqueId = gUnusedUniqueIds.top();
		gUnusedUniqueIds.pop();
	}
	gUidBasedList[uniqueId] = this;

	AddItemToContainerIfRequired(this);

	gItems.push_back(this);
}

CItem::CItem() : itemId(""), itemLevel(0), itemContainerA(0), itemContainerB(0)
{
	CreateItem();
}

CItem::CItem(string id) : itemId(id), itemLevel(1), itemContainerA(0), itemContainerB(0)
{
	CreateItem();
}

CItem::CItem(string id, int count) : itemId(id), itemLevel(1), itemContainerA(0), itemContainerB(0)
{
	CreateItem();
}

CItem::CItem(string id, int count, int level) : itemId(id), itemLevel(level), itemContainerA(0), itemContainerB(0)
{
	CreateItem();
}

CItem::~CItem(void)
{
}
