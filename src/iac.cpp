#include "iac.h"
#include "Container.h"

#include <sampgdk/sampgdk.h>

PLUGIN_EXPORT bool PLUGIN_CALL OnGameModeInit() {

  return true;
}

void AddItemToContainerIfRequired(CItem * item) {
	if(item->getContainer() != NULL) {
		gContainers[make_pair(item->getA(), item->getB())].Add(item, item->getA(), item->getB(), true);
	}
}

char * AmxGetString(AMX* amx, cell * params, int index) {
	// Get the length
	int iLength = 0;
	cell *pAddress = NULL;
	amx_GetAddr(amx, params[index], &pAddress); 
    amx_StrLen(pAddress, &iLength);
	// Make sure the length is valid
	if(!iLength)
		return 0;
	
	// Make room for the string end
	iLength++;
	// Allocate the string
	char *ret = new char [iLength];
	// Get the string
	amx_GetString(ret, pAddress, 0, iLength);
	return ret;
}

void SaveContainer(CContainer &inv, const string &dest) {
	ofstream ofs(dest.c_str());
	boost::archive::text_oarchive oa(ofs);
	oa & inv;
}

void LoadContainer(CContainer &inv, const string &filename) {
	std::ifstream ifs(filename.c_str());
	boost::archive::text_iarchive ia(ifs);
	ia >> inv;
}

cell AMX_NATIVE_CALL  SaveInventory(AMX* amx, cell* params)
{
	int playerid = (int)params[1];
	if(playerid != INVALID_PLAYER_ID) {
		char szName[MAX_PLAYER_NAME];

		GetPlayerName(playerid, szName, MAX_PLAYER_NAME);

		string Name(szName);

		SaveContainer(gContainers[make_pair(0, playerid)], "Containers/Player/" + Name + ".dat");
		return 1;
	}
	return 0;
}
cell AMX_NATIVE_CALL  LoadInventory(AMX* amx, cell* params)
{
	int playerid = (int)params[1];
	if(playerid != INVALID_PLAYER_ID) {
		char szName[MAX_PLAYER_NAME];

		GetPlayerName(playerid, szName, MAX_PLAYER_NAME);

		string Name(szName);

		LoadContainer(gContainers[make_pair(0, playerid)], "Containers/Player/" + Name + ".dat");
		return 1;
	}
	return 0;
}

cell AMX_NATIVE_CALL  DeleteContainer(AMX* amx, cell* params)
{
	int type = (int)params[1];
	int id = (int)params[2];

	gContainers[make_pair(type, id)].Delete();

	return 1;
}
cell AMX_NATIVE_CALL  SaveContainer(AMX* amx, cell* params)
{
	int type = (int)params[1];
	int id = (int)params[2];

	switch(type) {
		case 1: {
			string dir = "Containers/Shop/";
			string file = to_string(id) + ".dat";
			dir += file;
			SaveContainer(gContainers[make_pair(type, id)], dir.c_str());
			return 1;
		}
		case 2: {
			string dir = "Containers/Vehicle/";
			string file = id + ".dat";
			dir += file;
			SaveContainer(gContainers[make_pair(type, id)], dir.c_str());
			return 1;
		}
		default: {
			string dir = "Containers/" + type; dir += "/"; // lol wtf
			string file = id + ".dat";
			dir += file;
			SaveContainer(gContainers[make_pair(type, id)], dir.c_str());
			return 1;
		}
	}
	return 0;
}
cell AMX_NATIVE_CALL  LoadContainer(AMX* amx, cell* params)
{
	int type = (int)params[1];
	int id = (int)params[2];

	switch(type) {
		case 1: {
			string dir = "Containers/Shop/";
			string file = to_string(id) + ".dat";
			dir += file;
			LoadContainer(gContainers[make_pair(type, id)], dir.c_str());

			return 1;
		}
		case 2: {
			string dir = "Containers/Vehicle/";
			string file = to_string(id) + ".dat";
			dir += file;
			LoadContainer(gContainers[make_pair(type, id)], dir.c_str());
			return 1;
		}
		default: {
			string dir = "Containers/" + type; dir += "/"; // lol wtf
			string file = to_string(id) + ".dat";
			dir += file;
			LoadContainer(gContainers[make_pair(type, id)], dir.c_str());
			return 1;
		}
	}
	return 0;
}
cell AMX_NATIVE_CALL  AddItemToContainer(AMX* amx, cell* params)
{
	int type = (int)params[1];
	int id = (int)params[2];
	CItem * item = CItem::GetItemByUID((unsigned int)params[3]);
	if(item != NULL) {
		gContainers[make_pair(type, id)].Add(item, (int)type, id);
		return 1;
	}
	return 0;
}
cell AMX_NATIVE_CALL  RemoveItemFromContainer(AMX* amx, cell* params)
{
	int type = (int)params[1];
	int id = (int)params[2];
	CItem * item = CItem::GetItemByUID((unsigned int)params[3]);
	if(item != NULL) {
		gContainers[make_pair(type, id)].Remove(item);
		return 1;
	}
	return 0;
}

cell AMX_NATIVE_CALL  CountItemsInContainer(AMX* amx, cell* params)
{
	int type = (int)params[1];
	int id = (int)params[2];
	return gContainers[make_pair(type, id)].getCount();
}

cell AMX_NATIVE_CALL IsItemInContainer(AMX* amx, cell* params)
{
	int type = (int)params[1];
	int id = (int)params[2];
	char * item = AmxGetString(amx, params, 3);
	return gContainers[make_pair(type, id)].Find(string(item)) != NULL;
}

cell AMX_NATIVE_CALL FindItemInContainer(AMX* amx, cell* params)
{
	int type = (int)params[1];
	int id = (int)params[2];
	char * item = AmxGetString(amx, params, 3);
	int nth = 0;
	if(params[0] > 3) {
		nth = (int)params[4];
	}
	return gContainers[make_pair(type, id)].Find(string(item), nth)->getUniqueId();
}

cell AMX_NATIVE_CALL GetItemInContainer(AMX* amx, cell* params)
{
	int type = (int)params[1];
	int id = (int)params[2];
	int idx = (int)params[3];

	return gContainers[make_pair(type, id)].GetAt(idx);
}

cell AMX_NATIVE_CALL  DeleteItem(AMX* amx, cell* params)
{
	if((int)params[1]) {
		CItem * ptr = CItem::GetItemByUID((unsigned int)params[1]);
		if(ptr != NULL) {
			CItem::DeleteItem(ptr);
			return 1;
		}
	}
	return 0;
}
cell AMX_NATIVE_CALL  CreateItem(AMX* amx, cell* params)
{
	char * szName = AmxGetString(amx, params, 1);
	if(szName == NULL) return NULL;

	string name(szName);

	CItem * item = new CItem(name, (int)params[2]);
	if(item == NULL) return NULL;

	return item->getUniqueId();
}
cell AMX_NATIVE_CALL  GetItemId(AMX* amx, cell* params)
{
	CItem * ptr = CItem::GetItemByUID((unsigned int)params[1]);
	if(ptr != NULL) {
		cell* addr = NULL;

		amx_GetAddr(amx, params[2], &addr);
		amx_SetString(addr, ptr->getId().c_str(), 0, 0, params[2]);

		return 1;
	}
	return 0;
}

cell AMX_NATIVE_CALL  GetItemType(AMX* amx, cell* params)
{
	int uid = (int)params[1];
	CItem * ptr = CItem::GetItemByUID(uid);
	if(ptr != NULL) {
		return ptr->getType();
	}
	return NULL;
}
cell AMX_NATIVE_CALL  GetItemLevel(AMX* amx, cell* params)
{
	int uid = (int)params[1];
	CItem * ptr = CItem::GetItemByUID(uid);
	if(ptr != NULL) {
		return ptr->getLevel();
	}
	return NULL;

}
cell AMX_NATIVE_CALL  GetItemEffect(AMX* amx, cell* params)
{
	int uid = (int)params[1];
	int effect = (int)params[2];
	CItem * ptr = CItem::GetItemByUID(uid);
	if(ptr != NULL) {
		return ptr->getEffect(effect);
	}
	return NULL;
}
cell AMX_NATIVE_CALL  CountItemEffects(AMX* amx, cell* params)
{
	int uid = (int)params[1];
	CItem * ptr = CItem::GetItemByUID(uid);
	if(ptr != NULL) {
		return ptr->countEffects();
	}
	return NULL;
}
cell AMX_NATIVE_CALL  GetItemInfo(AMX* amx, cell* params)
{
	int uid = (int)params[1];
	char * info = AmxGetString(amx, params, 2);
	CItem * ptr = CItem::GetItemByUID(uid);
	if(ptr != NULL && info != NULL) {
		return ptr->getInfo(info);
	}
	return NULL;
}

cell AMX_NATIVE_CALL  SetItemType(AMX* amx, cell* params)
{
	int uid = (int)params[1];
	int type = (int)params[2];
	CItem * ptr = CItem::GetItemByUID(uid);
	if(ptr != NULL) {
		ptr->setType(type);
		return 1;
	}
	return 0;
}
cell AMX_NATIVE_CALL  SetItemLevel(AMX* amx, cell* params)
{
	int uid = (int)params[1];
	int level = (int)params[2];
	CItem * ptr = CItem::GetItemByUID(uid);
	if(ptr != NULL) {
		ptr->setLevel(level);
		return 1;
	}
	return 0;
}
cell AMX_NATIVE_CALL  SetItemEffect(AMX* amx, cell* params)
{
	int uid = (int)params[1];
	int effect = (int)params[2];
	int value = (int)params[3];
	CItem * ptr = CItem::GetItemByUID(uid);
	if(ptr != NULL) {
		ptr->setEffect(effect, value);
		return 1;
	}
	return 0;
}
cell AMX_NATIVE_CALL  SetItemInfo(AMX* amx, cell* params)
{
	int uid = (int)params[1];
	char * info = AmxGetString(amx, params, 2);
	int value = (int)params[3];
	CItem * ptr = CItem::GetItemByUID(uid);
	if(ptr != NULL) {
		ptr->setInfo(info, value);
		return 1;
	}
	return 0;
}


AMX_NATIVE_INFO PluginNatives[] =
{
    {"CreateItem", CreateItem},
    {"DeleteItem", DeleteItem},

	{"GetItemName", GetItemId},
    {"GetItemType", GetItemType},
    {"GetItemLevel", GetItemLevel},
    {"GetItemEffect", GetItemEffect},
    {"CountItemEffects", CountItemEffects},
    {"GetItemInfo", GetItemInfo},
    {"SetItemType", SetItemType},
    {"SetItemLevel", SetItemLevel},
    {"SetItemEffect", SetItemEffect},
    {"SetItemInfo", SetItemInfo},

	{"SaveInventory", SaveInventory},
	{"LoadInventory", LoadInventory},

	{"DeleteContainer", DeleteContainer},
	{"SaveContainer", SaveContainer},
	{"LoadContainer", LoadContainer},
	{"AddItemToContainer", AddItemToContainer},
	{"RemoveItemFromContainer", RemoveItemFromContainer},
	{"CountItemsInContainer", CountItemsInContainer},
	{"IsItemInContainer", IsItemInContainer},
	{"FindItemInContainer", FindItemInContainer},
	{"GetItemInContainer", GetItemInContainer},
	{ 0, 0 }
};









PLUGIN_EXPORT unsigned int PLUGIN_CALL Supports() {
  return sampgdk::Supports() | SUPPORTS_PROCESS_TICK | SUPPORTS_AMX_NATIVES;
}

extern void *pAMXFunctions;

PLUGIN_EXPORT bool PLUGIN_CALL Load(void **ppData) {
  pAMXFunctions = ppData[PLUGIN_DATA_AMX_EXPORTS];
  return sampgdk::Load(ppData);
}

PLUGIN_EXPORT int PLUGIN_CALL AmxLoad(AMX *amx)
{
	return amx_Register(amx, PluginNatives, -1);
}

PLUGIN_EXPORT int PLUGIN_CALL AmxUnload(AMX *amx)
{
	return true;
}

PLUGIN_EXPORT void PLUGIN_CALL Unload() {
	sampgdk::Unload();
}

PLUGIN_EXPORT void PLUGIN_CALL ProcessTick() {
  sampgdk::ProcessTick();
}
